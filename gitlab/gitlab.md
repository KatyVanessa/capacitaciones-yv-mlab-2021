#   **MENU PRINCIPAL**
* ### [Biografia](https://gitlab.com/KatyVanessa/capacitaciones-yv-mlab-2021/-/blob/master/biografia/biografia.md).
* ###  [Ir a Git](https://gitlab.com/KatyVanessa/capacitaciones-yv-mlab-2021/-/blob/master/git/git.md).
* ###  [Ir a Inicio](https://gitlab.com/KatyVanessa/capacitaciones-yv-mlab-2021/-/blob/master/README.md).





#   **MENU**
1. [Gitlab](#Gitlab)
2. [Crear-Repositorios](#Crear-Repositorios)
3. [Crear-Grupos](#Crear-Grupos)
4. [Crear-Sub-grupos](#Crear-Sub-grupos)
5. [Crear-Issues](#Crear-Issues)
6. [Crear-Labels](#Crear-Labels)
7. [Permisos-y-Agregar-miembros](#Permisos-y-Agregar-miembros)
8. [Agregar-miembros](#Agregar-miembros)
9. [Crear-Boards](#Crear-Boards)

#   *Gitlab*

<img src="./imagenes/gitlab.png" height="200px" width="300px">

_GitLab es la primera aplicación única para el desarrollo de software, la seguridad y las operaciones que habilita DevOps concurrentes. GitLab acelera el ciclo de vida del software y mejora radicalmente la velocidad del negocio._

<img src="./imagenes/instalacion.jpeg" height="200px" width="300px">

_Si ya tenemos la cuenta institucional es mas facil ya que se loguea con los mismos datos._

#   *Crear Repositorios*

<img src="./imagenes/repositorio.jpeg" height="200px" width="300px">

_Crea un nuevo repositorio
git clone https://gitlab.com/KatyVanessa/capacitaciones-yv-mlab-2021.git
cd capacitaciones-yv-mlab-2021
touch README.md_

#   *Crear Grupos*

<img src="./imagenes/grupos.jpeg" height="200px" width="300px">

_La creación de un grupo conecta varios repositorios y permite a los miembros acceder al proyecto otorgando_

<img src="./imagenes/crear-grupo.jpeg" height="200px" width="300px">

_Debemos llenar los datos y dar acceso a las personas que deben estar en el grupo._
_Podemos incluir a las personas que deseemos al grupo_


#   *Crear Sub grupos*

<img src="./imagenes/sub-grupo.jpeg" height="200px" width="300px">

_A partir de los grupos podemos crear los subgrupos y asi tambien elegir a las personas que van en cada grupo o sub grupo_

#   *Crear Issues*

<img src="./imagenes/Isuess.jpeg" height="200px" width="300px">

_Se crean las actividades que se van a realziar durante el proceso, y si van culmimando van pasando a **done**_

#   *Crear Labels*

<img src="./imagenes/labels.jpeg" height="200px" width="300px">

_Se van creando y asignando las tareas a realizar o las tareas cumplidas._

#   *Permisos y agregar miembros*    

<img src="./imagenes/permisos.jpeg" height="200px" width="300px">

_Tener ciertas opciones habilitadas en el proyecto y versionamiento_

#   *Crear Boards*

<img src="./imagenes/boards.jpeg" height="200px" width="300px">

_Se utiliza para planificar, organizar y visualizar un flujo de trabajo para una función o lanzamiento de un producto._


 

 