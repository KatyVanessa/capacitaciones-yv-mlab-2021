#   **MENU PRINCIPAL**
* ### [Biografia](https://gitlab.com/KatyVanessa/capacitaciones-yv-mlab-2021/-/blob/master/biografia/biografia.md).

* ###  [Regresar a Inicio](https://gitlab.com/KatyVanessa/capacitaciones-yv-mlab-2021/-/blob/master/README.md).

* ###  [Ir a Gitlab](https://gitlab.com/KatyVanessa/capacitaciones-yv-mlab-2021/-/blob/master/gitlab/gitlab.md).




#   **MENU**
1. [Que es Git](#Que-es-git)
2. [Comandos de Git](#Comandos-de-Git)
3. [Clientes Git](#Clientes-Git)
4. [Clonacion de proyecto](#Clonacion-de-proyecto)
5. [Commits por consola y cliente Kraken](#Commits-por-consola-y-cliente-kraken)
6. [Ramas desde kraken](#Ramas-desde-kraken)
7. [Marge](#Marge)


#   *Que es Git*
_Git es una herramienta que realiza una función del control de versiones de código de forma distribuida, de la que destacamos varias características:_

- Es muy potente

- No depende de un repositorio central

- Es software libre
- Con ella podemos mantener un historial completo de versiones
Podemos movernos, como si tuviéramos un puntero en el tiempo, por todas las revisiones de código y desplazarnos una manera muy ágil.

#   *Comandos de Git*
**git init**

_Podemos ejecutar ese comando para crear localmente un repositorio con GIT y así utilizar todo el funcionamiento que GIT ofrece.  Basta con estar ubicados dentro de la carpeta donde tenemos nuestro proyecto y ejecutar el comando._

**git commit -m "mensaje" + archivos**

_Hace commit a los archivos que indiquemos, de esta manera quedan guardados nuestras modificaciones._

**git branch**

_Nos muestra una lista de los branches que existen en nuestro repositorio._

**git merge NombreDeBranch**

_Hace un merge entre dos branches, en este caso la dirección del merge sería entre el branch que indiquemos en el comando, y el branch donde estémos ubicados._

**git clone URL/name.git NombreProyecto**

_Clona un proyecto de git en la carpeta NombreProyecto._


#   *Clientes Git*

_También hay clientes Git GUI que funcionan bien en plataformas Windows. Hemos elegido los mejores para ti:_

**GitHub**
_Si tu repositorio remoto está en GitHub, entonces esta herramienta será la más útil para ti. El software es básicamente una extensión de tu flujo de trabajo en GitHub. Simplemente inicia sesión con tu cuenta de GitHub y ya podrás comenzar a trabajar en tus repositorios._ 

#   *Clonacion de proyecto*

<img src="./imagenes/clonar.jpeg" height="200px" width="300px">

- En Gitlab, visita la página principal del repositorio.

- Sobre la lista de archivos, da clic en  Código.

- Para clonar el repositorio usando HTTPS, en "Clonar con HTTPS", haga clic en. 

- Para clonar el repositorio con una clave SSH, incluido un certificado emitido por la autoridad de certificación SSH de su organización, haga clic en Usar SSH y luego copie el link del proyecto

- Abre la Git Bash.

- Cambia el directorio de trabajo actual a la ubicación en donde        quieres clonar el directorio.

- Escribe git clone, y luego pega la URL que copiaste antes.

#   *Commits por consola y cliente Kraken*

<img src="./imagenes/commit.jpeg" height="200px" width="300px">

_En el caso que otro desarrollador generará un commit por ejemplo en la rama “master” del repositorio remoto._


#   *Ramas desde kraken*
<img src="./imagenes/commit.jpeg" height="200px" width="300px">

_Las branches o rama corresponden a distintos conjuntos de cambios apilados bajo un nombre común. Se puede visualizar como una versión alternativa de tu repositorio, utilizado principalmente para guiar lineas de trabajo y así ordenar cambios._





